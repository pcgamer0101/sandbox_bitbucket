from django.contrib import admin
from .models import TaskListItem

admin.site.register(TaskListItem)
