from django.db import *
from django.contrib.auth.models import *
from django.db.models.signals import *
from django.core.exceptions import ObjectDoesNotExist

class TaskListItem(models.Model):
    title = models.CharField(max_length=50)
    order = models.PositiveIntegerField(blank=True)

    def __str__(self):
        return self.title
