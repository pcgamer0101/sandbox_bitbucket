
$( document ).ready(function() {
    $("#sortable").sortable({

        start: function(e, ui) {
            // creates a temporary attribute on the element with the old index
            $(this).attr('data-previndex', ui.item.index());
        },

        update: function(e, ui) {
            // gets the new and old index then removes the temporary attribute
            var newIndex = ui.item.index();
            var oldIndex = $(this).attr('data-previndex');
            var element_id = ui.item.attr('id');
            //alert('id of Item moved = '+element_id+' old position = '+oldIndex+' new position = '+newIndex);
            $(this).removeAttr('data-previndex');

            //var order = $(this).sortable('toArray');
            var order = $(this).sortable("serialize", { key: "itemid" });
            //var username = newIndex;
            var username = order;

            $.ajax({
                type: "POST",
                url: '/primaryapp/jsonReturn/',
                data: JSON.stringify({'username': username}),
                dataType: 'json',
                contentType: 'text/plain',
                success: function (data) {
                  if (data.username) {
                    //alert("Got data back from the server.");
                    $( "p" ).text( data.username );
                  }
                }
            });
        }
    });
});

$("#sortable").disableSelection();