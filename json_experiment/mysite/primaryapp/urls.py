from django.conf.urls import url
from . import views

app_name = 'primaryapp'

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^jsonReturn/$', views.jsonReturn, name='jsonReturn')
]