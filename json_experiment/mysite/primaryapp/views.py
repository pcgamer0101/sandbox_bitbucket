from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.http import require_POST
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from .models import *
from django.db.models import Q
import json
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
import re
import time

def index(request):
    print('index')
    #tasks = TaskListItem.objects.all()
    tasks = TaskListItem.objects.all().order_by('order')
    return render(request, 'primaryapp/login.html', {'tasks': tasks})

@csrf_exempt
def jsonReturn(request):
    print('jsonReturn')
    start = time.time()
    if request.method == 'GET':
        print('Entered GET if statement')
        #json_data = json.loads(request.body)
        json_data = request.body
        print(json_data)

        data = {'username': 'dummydata'}
        return JsonResponse(data)

    elif request.method == 'POST':
        print('Entered POST if statement')
        start = time.clock()

        json_data = request.body
        print(json_data)
        json_obj = json.loads(json_data)
        print(json_obj)

        #print(json_obj["username"])
        item_id = json_obj

        # print(item_id)
        # print(item_id[1])
        print(isinstance(item_id[1], str))

        item_id_int = [int(i) for i in item_id]
        # print(isinstance(item_id_int[1], int))
        # print(item_id_int)
        # print(len(item_id_int))

        #tasks = TaskListItem.objects.all().order_by('order')

        #print(tasks.count())  # maybe do something with the queryset

        for i in range(len(item_id_int)):
            # print(i)
            item = TaskListItem.objects.get(pk=item_id_int[i])
            item.order = i
            item.save()
            # print('in the for loop')
            # print(i)
            i = i+1


        #json_list = list(json_obj.values())

        elapsed = (time.clock() - start)
        print(elapsed)
        data = {"username": 'After AJAX'}
        return JsonResponse(data)

    #return render(request, 'primaryapp/login.html')







# python_dict = {'name': 'Bob'}
# dictToJson = json.dumps(python_dict)
# print(dictToJson)
# json_obj = json.loads(dictToJson)
# print(json_obj)


        # for i in range(len(item_id_int)):
        #     # print(i)
        #     item = TaskListItem.objects.get(pk=item_id_int[i])
        #     item.order = i
        #     item.save()
        #     # print('in the for loop')
        #     # print(i)
        #     i = i+1